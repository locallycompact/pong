module Main


import Control.Linear.LIO
import Data.IORef
import Data.Nat
import Data.Vect
import System.Clock
import System.FFI
import System.Random

import System
import SDL
import SDL.Font
import SDL.Foreign

import Data.SortedMap
import Data.HVect
import Data.Vect.Elem
import SDL.Font.Foreign

%auto_implicit_depth 256
black : SDLColor
black = RGBA 0 0 0 255

red : SDLColor
red = RGBA 255 0 0 255
%auto_implicit_depth 50

data Ref : (l : label) -> Type -> Type where
  [search l]
  MkRef : IORef a -> Ref x a

export
newRef : HasIO io => (x : label) -> t -> io (Ref x t)
newRef x val = do ref <- liftIO $ newIORef val
                  pure (MkRef ref)

get : HasIO io => (x : label) -> (ref : Ref x a) => io a
get x {ref = MkRef io} = liftIO $ readIORef io

export %inline
put : HasIO io => (x : label) -> (ref : Ref x a) => a -> io ()
put x {ref = MkRef io} val = liftIO $ writeIORef io val

export %inline
modify : HasIO io => (x : label) -> (ref : Ref x a) => (a -> a) -> io ()
modify x f = do ref <- get x
                put x (f ref)

putError : LinearIO io => (err : SDLError) -> L io ()
putError = putStrLn . show

putFontError : LinearIO io => (err : SDLFontError) -> L io ()
putFontError = putStrLn . show

data TxtM : Type where

K : Type
K = Ref TxtM (SortedMap String (SDLTexture, SDLRect))

lookupTexture : (HasIO io, K) => String -> io (Maybe (SDLTexture, SDLRect))
lookupTexture x = do
  k <- get TxtM
  pure (lookup x k)

f : LinearIO io => Maybe (SDLTexture, SDLRect) -> (1 _ : SDL WithRenderer) -> L {use = 1} io (SDLErrorPath WithRenderer WithRenderer)
f (Just (a, l)) s = copySurface a l l s
f Nothing s = ?p

withSimpleImage : (LinearIO io, K) => String -> String -> SDLRect -> ((1 _ : SDL WithRenderer) -> L { use = 1} io (SDLErrorPath WithRenderer WithRenderer)) -> (1 _ : SDL WithRenderer) -> L { use = 1 }  io (SDLErrorPath WithRenderer WithRenderer)
withSimpleImage f x r a s = withImage f (\(Texture h) => \s => do
                             modify TxtM (insert x (Texture h, r))
                             a s) s

drawGame : (LinearIO io, K)
        => (1 _ : SDL WithRenderer)
        -> L {use = 1} io (SDL WithRenderer)
drawGame x = do
  Success s <- withSimpleImage "logo.png" "foo" (MkRect 0 0 100 100) (\s => do
                 b <- Main.get TxtM
                 let x = lookup "foo" b
                 f x s
                ) x
    | Failure s err => do putError err
                          pure1 s
  pure1 s

eventLoop : (LinearIO io, K)
         => (1 _ : SDL WithRenderer)
         -> L {use = 1} io (SDL WithRenderer)
eventLoop s = do
    now <- liftIO $ clockTime Monotonic
    usleep 100000
    evt <- pollEvent
    s <- drawGame s
    s <- render s
    eventLoop s


defaultWindowOpts : SDLWindowOptions
defaultWindowOpts =
  MkSDLWindowOptions { name = "Example"
                     , x = SDLWindowPosCentered
                     , y = SDLWindowPosCentered
                     , width = 500
                     , height = 500
                     , flags = []
                     }

ab : LinearIO io => Either SDLError SDLFont -> L io ()
ab x = case x of 
          Left a => putStrLn (show a) >> pure ()
          Right b => pure ()

doFont' : LinearIO io => (1 _ : SDL WithRenderer) -> (1 _ : SDLTTF Inited) -> L { use = 1} io (SDLTTFErrorPath' [WithRenderer, Inited] [WithRenderer, Inited])
doFont' k Initial = withFont' "Montserrat-Light.otf" 12 (\(Font ptr) => \k => do
               p <- withSolidSurfaceRender (Font ptr) "foo" red (
                     \(Texture h) => \s => copySurface (Texture h) (MkRect 0 0 100 100) (MkRect 0 0 100 100) s
                  ) k

               pure1 p) (WithSDLAndTTF k Initial)

doFont : LinearIO io => (1 _ : SDL WithRenderer) -> L io {use = 1} (SDL WithRenderer)
doFont b = initSDLTTF' b (\s => \err => putStrLn "Fatal error: \{show err}" >> pure1 s ) (\b => \k => do
      Success k <- doFont' b k
        | Failure s err => ?dfsdf
      putStrLn "fo"
      quitSDLTTF' k)

win : (Applicative io, LinearIO io, K) => L io ()
win = initSDL [SDLInitVideo] (\err => putStrLn "Fatal error: \{show err}") $ \s => do
  putStrLn "=> SDL Inited"

  let winops : SDLWindowOptions = record { flags = [SDLWindowMaximized] } defaultWindowOpts
  Success s <- newWindow winops s
    | Failure s err => handleInitedError s (putError err)
  putStrLn "=> Window created"

  Success s <- newRenderer Nothing [SDLRendererSoftware] s
    | Failure s err => handleWindowedError s (putError err)
  putStrLn "=> Renderer operational"

  s <- doFont s

  s <- drawGame s
  s <- render s
  s <- eventLoop s

  s <- closeRenderer s
  putStrLn "=> Renderer closed"
  s <- closeWindow s
  putStrLn "=> Window closed"
  quitSDL s
  putStrLn "=> SDL quitted"

main : IO ()
main = do
  clock <- clockTime Monotonic
  Control.Linear.LIO.run $ do
    ref <- newRef TxtM empty
    win
