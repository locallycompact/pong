{
  inputs.userpkgs.url = github:locallycompact/nixpkgs/1a57ed16dd683cabddf969be2ebf6edc8457f32e;
  inputs.flake-utils.url = github:numtide/flake-utils;
  outputs = { self, userpkgs, flake-utils } :
    flake-utils.lib.eachSystem [ "x86_64-linux" ] (system:
      let pkgs = import userpkgs {inherit system; };
          sdl = pkgs.idris2.callPackage' ./sdl.nix {};
          p = (pkgs.idris2.withPkgs (ps: [ ps.comonad sdl ps.idrall]) );
      in {
        devShell = pkgs.mkShell {
          buildInputs = [ p ];
          shellHook=''
            export LD_LIBRARY_PATH="${p}/idris2-0.3.0/sdl-0.1.0/lib:$LD_LIBRARY_PATH"
            '';
        };
      }
    );
}
