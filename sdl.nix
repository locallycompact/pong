{ buildIdris, fetchFromGitHub, SDL2, SDL2_image, SDL2_ttf}:

let sources = import ./nix/sources.nix {};
in
buildIdris {

  name = "sdl";

  extraBuildInputs = [SDL2 SDL2_image SDL2_ttf];

  src = sources.idris2-sdl;

}
